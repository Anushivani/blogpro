import Navbar from "./components/Navbar/Navbar";
import { BrowserRouter , Routes,Route } from "react-router-dom";
import Profile from "./components/Navbar/Profile";
import More from "./components/Navbar/More";
import Compose from "./components/Navbar/Compose";
import { useHistory } from 'react-router-dom'; 
import Layout from "./components/Layout/Layout";

function App() {
 return(
  <div>
          {/* <Navbar/> */}
          {/* <BrowserRouter></BrowserRouter> */}
          <Layout/>
          <Routes>
            
            {/* <Route   path="/" component={Navbar} exact={true} /> */}
            <Route  path="/More" component={More} exact={true} />
            <Route  path="/Profile" component={Profile} exact={true} />
            <Route  path="/Compose" component={Compose} exact={true} />
          </Routes>
  </div>
 );
}

export default App;
