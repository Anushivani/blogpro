import Navbar from "../Navbar/Navbar";
import Main from "../Main/Main";
const Layout=()=>{
    return(
        <div>
            <Navbar/>
            <Main/>
        </div>
    );
}
export default Layout;