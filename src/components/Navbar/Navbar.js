import styles from "./Navbar.module.css";
//import history from './history';
//import { useHistory } from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom';
import More from "./More";
import React from "react";
//import Card  from "../Card/Card";
const Navbar=()=>{
    // let history = useHistory();
    const navigate = useNavigate();
    const GotoMore = () => {
        navigate('/More');
    //    history.push ('./More');
    }  
    const GotoProfile=()=>{
        navigate('/Profile');
    }  
    const GotoCompose=()=>{
        navigate('/Compose');
    }  
    return(
        <div className={styles.navbar}>
            
               <div> <h1>POST-O-BLOG</h1></div>
                <div className={styles.btnclass}>
                <button onClick={GotoMore} className={styles.btn}>More</button>
                <button onClick={GotoProfile} className={styles.btn}>Profile</button>
                <button className={styles.btn}>Logout</button>
                <button onClick={GotoCompose} className={styles.btn}>Compose</button>
                </div>
                {/* <Card/> */}
        
        </div>
    );
}
export default Navbar;